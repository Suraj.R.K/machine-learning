library(datasets)
str(attitude)
summary(attitude)
dat=attitude[,c(1,5)]
plot(dat, main = "% of favourable responses to
     Learning and Privilege", pch =20, cex =2)
set.seed(7)
km1=kmeans(dat,3,nstart=1)
plot(dat, col=(km1$cluster+1 ),main="kmeans clustering with 3 clusters" , pch=20,cex=2)